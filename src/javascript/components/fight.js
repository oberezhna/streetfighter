import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner';

export async function fight(firstFighter, secondFighter) {
  let leftFighterHealth = document.getElementById('left-fighter-indicator');
  let rightFighterHealth = document.getElementById('right-fighter-indicator');

  leftFighterHealth.style.width = '100%';
  rightFighterHealth.style.width = '100%';
  let leftFighterHealthCounter = firstFighter.health;
  let rightFighterHealthCounter = secondFighter.health;

  const { PlayerOneAttack, PlayerOneBlock, PlayerTwoAttack, PlayerTwoBlock } = controls;
  const PlayerOneCriticalHitCombination=controls.PlayerOneCriticalHitCombination;
  const PlayerTwoCriticalHitCombination=controls.PlayerTwoCriticalHitCombination;
  
 
  return new Promise((resolve) => {
    let activeButtons=[];
    document.addEventListener('keydown', function(e) {
      let button = e.keyCode;
      activeButtons.push(button);
      if(button === PlayerOneAttack){
        getHitPower(firstFighter);
         rightFighterHealthCounter 
        if(activeButtons[PlayerTwoBlock]){
          getBlockPower(secondFighter);
        }
      }
    });
    // resolve the promise with the winner when fight is over
    if (leftFighterHealthCounter <= 0) return resolve(rightFighter);
    if (rightFighterHealthCounter <= 0) return resolve(leftFighter);
  })
  .then(res=>showWinnerModal(res))
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  // return damage
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random(1,2);
  let hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  dodgeChance = Math.random(1,2);
  let blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

function criticalHit(fighter) {
  let criticalHit = fighter.attack*2
  return criticalHit;
}